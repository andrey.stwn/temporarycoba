package id.atiila.creditdivision;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class Applicant implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label("ID Transaksi")
	private long idTransaksi;
	private Map<String, Object> items = new HashMap<>();

	public Applicant() {
	}

	public long getIdTransaksi() {
		return this.idTransaksi;
	}

	public void setIdTransaksi(long idTransaksi) {
		this.idTransaksi = idTransaksi;
	}

	public Map<String, Object> getItems() {
		return items;
	}

	public void setItems(Map<String, Object> items) {
		this.items = items;
	}

	public Integer valueInt(String key) {
		return (Integer) items.get(key);
	}

	public String value(String key) {
		return (String) items.get(key);
	}

	public Date valueDate(String key) {
		ZonedDateTime value = ZonedDateTime.parse((String) items.get(key));
		return Date.from(value.toInstant());
	}

	public Applicant(long idTransaksi) {
		this.idTransaksi = idTransaksi;
	}
	
	public void putMapItem(String varKey, Object varValue){
        this.items.put(varKey, varValue);
    }

}