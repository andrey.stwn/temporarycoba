package id.atiila.creditdivision;

import java.io.Serializable;

public class BaseRule implements Serializable {
	private Boolean resultBoolean;
	private Integer resultScore;
	private Integer resultDeviasi;

	@org.kie.api.definition.type.Label("Result Scoring DP")
	private java.lang.Integer resultScoringDP;

	@org.kie.api.definition.type.Label(value = "Result Scoring Area")
	private java.lang.Integer resultScoringArea;

	public Boolean getResultBoolean() {
		return resultBoolean;
	}

	public void setResultBoolean(Boolean resultBoolean) {
		this.resultBoolean = resultBoolean;
	}

	public Integer getResultScore() {
		return resultScore;
	}

	public void setResultScore(Integer resultScore) {
		this.resultScore = resultScore;
	}

	public Integer getResultDeviasi() {
		return resultDeviasi;
	}

	public void setResultDeviasi(Integer resultDeviasi) {
		this.resultDeviasi = resultDeviasi;
	}

	public BaseRule() {
	}

	public java.lang.Integer getResultScoringDP() {
		return this.resultScoringDP;
	}

	public void setResultScoringDP(java.lang.Integer resultScoringDP) {
		this.resultScoringDP = resultScoringDP;
	}

	public java.lang.Integer getResultScoringArea() {
		return this.resultScoringArea;
	}

	public void setResultScoringArea(java.lang.Integer resultScoringArea) {
		this.resultScoringArea = resultScoringArea;
	}

	public BaseRule(java.lang.Boolean resultBoolean,
			java.lang.Integer resultScore, java.lang.Integer resultDeviasi,
			java.lang.Integer resultScoringDP,
			java.lang.Integer resultScoringArea) {
		this.resultBoolean = resultBoolean;
		this.resultScore = resultScore;
		this.resultDeviasi = resultDeviasi;
		this.resultScoringDP = resultScoringDP;
		this.resultScoringArea = resultScoringArea;
	}
}
